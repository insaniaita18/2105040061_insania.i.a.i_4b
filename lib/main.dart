import 'package:flutter/material.dart';

void main() {
  runApp(const FungsiUtama());
}

class FungsiUtama extends StatelessWidget {
  const FungsiUtama({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
   return MaterialApp (
    home: Scaffold(
      appBar: AppBar(
          title : Text("App bar Hello Word!"),
        ) ,
        body: Center(child: Text("Hello World")),
        ));

  }
}
